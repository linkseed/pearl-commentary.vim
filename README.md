# commentary.vim for Pearl

commentary.vim: comment stuff out

## Details

- Plugin: https://github.com/tpope/vim-commentary
- Pearl: https://github.com/pearl-core/pearl
